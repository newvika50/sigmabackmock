package sigma.backmock.sigmabackmock;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserDto {
    @JsonProperty("id")
    Long id;
    @JsonProperty("username")
    String username;
    @JsonProperty("lastName")
    String lastName;
    @JsonProperty("firstName")
    String firstName;
    @JsonProperty("patronymic")
    String patronymic;
    @JsonProperty("phone")
    String phone;
    @JsonProperty("email")
    String email;
    @JsonProperty("info")
    String info;
    @JsonProperty("role")
    String role;
}
