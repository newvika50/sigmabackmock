package sigma.backmock.sigmabackmock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@CrossOrigin
@RestController
public class SimpleController {
    List<UserDto> myDBUsersLol = Collections.emptyList();
    List<TaskDto> myDBTasksLol = Collections.emptyList();

    @Autowired
    Helper helper;

    @RequestMapping
    public String helloWorld() {
        return "Hello World!";
    }


    @GetMapping("/candidates/{user_id}")
    public UserDto getUserDtoById(@PathVariable long user_id) {
        return helper.createNewUserDto(user_id, "Naaaame");
    }

    @GetMapping("/candidates")
    public List<UserDto> getUserDtos() {
        if (myDBUsersLol.isEmpty()) {
            myDBUsersLol = new ArrayList<>();
            myDBUsersLol.add(helper.createNewUserDto(1L, "qqqq"));
            myDBUsersLol.add(helper.createNewUserDto(2L, "dd"));
            myDBUsersLol.add(helper.createNewUserDto(2L, "dd"));
            myDBUsersLol.add(helper.createNewUserDto(4L, "gggg"));
            myDBUsersLol.add(helper.createNewUserDto(5L, "hhhhh"));
            myDBUsersLol.add(helper.createNewUserDto(6L, "jjjjj"));
        }
        return myDBUsersLol;
    }

    @PutMapping("/candidates/{user_id}")
    public UserDto putUserDtoById(@PathVariable long user_id, @RequestBody UserDto userDto) {
        userDto.setId(user_id);
        userDto.setFirstName(userDto.getFirstName() + "_CHANCHED_PUT");
        return userDto;
    }

    @PostMapping("/candidates")
    public UserDto postUserDtoById(@RequestBody UserDto userDto) {
        myDBUsersLol.add(userDto);
        userDto.setFirstName(userDto.getFirstName() + "_CHANCHED_POST");
        return userDto;
    }


    @GetMapping("/tasks/{task_id}")
    public TaskDto getTasksById(@PathVariable long task_id) {
        return helper.createNewTaskDto(task_id, "Task Naaaame");
    }

    @GetMapping("/tasks")
    public List<TaskDto> getTasks() {
        if (myDBTasksLol.isEmpty()) {
            myDBTasksLol = new ArrayList<>();
            myDBTasksLol.add(helper.createNewTaskDto(1L, "qqqq"));
            myDBTasksLol.add(helper.createNewTaskDto(2L, "www"));
            myDBTasksLol.add(helper.createNewTaskDto(3L, "eeee"));
            myDBTasksLol.add(helper.createNewTaskDto(4L, "rrrr"));
            myDBTasksLol.add(helper.createNewTaskDto(5L, "tttt"));
        }
        return myDBTasksLol;
    }

    @PutMapping("/tasks/{tasks_id}")
    public TaskDto putTasksById(@PathVariable long tasks_id, @RequestBody TaskDto task) {
        task.setId(tasks_id);
        task.setName(task.getName() + "_CHANCHED_PUT");
        return task;
    }

    @PostMapping("/tasks")
    public TaskDto postTasksById(@RequestBody TaskDto task) {
        myDBTasksLol.add(task);
        task.setName(task.getName() + "_CHANCHED_POST");
        return task;
    }

    @GetMapping("/me")
    public UserDto getMyProfile() {
        UserDto me = new UserDto();
        me.setFirstName("Иван");
        me.setLastName("Иванов");
        me.setPatronymic("Ивановичч");
        me.setInfo("Информация дополнительная бла-бла-бла");
        me.setEmail("email@email.com");
        me.setPhone("+98888888");
        me.setPhone("123");
        me.setUsername("usernameIvanov");
        return me;
    }

    @GetMapping("/my_tasks")
    public List<TaskDto> getMyTasks() {
        List<TaskDto> tasks = List.of(
                helper.createNewTaskDto(1L, "Task 1"),
                helper.createNewTaskDto(1L, "Task 3")
        );
        return tasks;
    }

}
