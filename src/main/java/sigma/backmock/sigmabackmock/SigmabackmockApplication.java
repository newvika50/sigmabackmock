package sigma.backmock.sigmabackmock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SigmabackmockApplication {

    public static void main(String[] args) {
        SpringApplication.run(SigmabackmockApplication.class, args);
    }

}
