package sigma.backmock.sigmabackmock;

import org.springframework.stereotype.Service;

@Service
public class Helper {
    UserDto createNewUserDto(Long id, String firstName) {
        UserDto userDto = new UserDto();
        userDto.setId(id);
        userDto.setFirstName(firstName);
        userDto.setLastName("setLastName");
        userDto.setPatronymic("setPatronymic");
        userDto.setInfo("Inffo");
        userDto.setRole("role");
        return userDto;
    }

    TaskDto createNewTaskDto(Long id, String name) {
        TaskDto taskDto = new TaskDto();
        taskDto.setDescription("description");
        taskDto.setName(name);
        taskDto.setId(id);
        taskDto.setTopic("topiccc Java");
        taskDto.setTests("tests");
        return taskDto;
    }
}
